#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import io
import requests
import sys

from lxml import etree
from pathlib import Path
from pprint import pprint

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent)
sys.path.append(BOOKMARKING_FOLDER)
from main_route_file import BM_MODULES_DIR
sys.path.append(BM_MODULES_DIR)
from main_route_file import UNESCO_DIR
sys.path.append(UNESCO_DIR)

from bm_traverse_modules.server import get_response

# SYNONYMS_DOMAIN_EXAMPLE = 'https://www.sinónimo.es/ciencias+sociales.html'
SYNONYMS_DOMAIN = 'https://www.sinónimo.es/'


def get_synonym_url_for(string_words):
    words = string_words.split()
    url_words = ''

    for word in words:
        url_words += word+'+'

    url_words = url_words[:-1]
    url_words += '.html'
    synonym_url = SYNONYMS_DOMAIN+url_words
    return synonym_url

def search_for_synonyms(string_words):
    synonym_url = get_synonym_url_for(string_words)
    server_response = get_response(synonym_url, 0)
    synonyms_list = []

    if (server_response is not None):
        encoding    = 'utf-8' 
        parser      = etree.HTMLParser(encoding = encoding)
        tree        = etree.parse(io.StringIO(server_response.text), parser)
        
        body_tag    = tree.find('body')
        syn_div     = body_tag.xpath("//div[@id='synonyms']")
        
        syn_div_items = syn_div[0].xpath("//div[@class='item']")

        for div_item in syn_div_items:
            joinned_texts = div_item.itertext()

            for joinned_text in joinned_texts:
                if joinned_text[0] == ' ': joinned_text = joinned_text[1:]
                texts = joinned_text.split(', ')
                synonyms_list.extend(texts)
      
    return synonyms_list

if __name__ == '__main__': 
    content = sys.argv
    content.pop(0)
    synonyms_list = search_for_synonyms(content[0])
    pprint(synonyms_list)
