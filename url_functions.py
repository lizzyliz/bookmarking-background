#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import re
import sys
from pathlib import Path
from pprint import pprint

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent)
sys.path.append(BOOKMARKING_FOLDER)
from main_route_file import BM_MODULES_DIR
sys.path.append(BM_MODULES_DIR)
from main_route_file import UNESCO_DIR
sys.path.append(UNESCO_DIR)

from bm_traverse_modules.server import get_response
from bm_traverse_modules.text_processing import word_has_extension
from unesco_loader.category_collection import CategoryCollection
from unesco_loader.categories_manager import categorize_text

from html_xhtml_web_page import HtmlXhtmlWebPage
from pdf_web_page        import PdfWebPage
from web_constants       import (html_extensions, most_used_domains, 
    protocols, google_string_redirection)
from console_constants   import (unicode_characters as uc, colors as c)


# private
def protocolize_and_validate_url(url):
    ''' Checks if url contains http protocol. If not, it tries accessing the url 
    content by adding the protocol. Verified url is the suggested and verified url.
    '''
    url_simple_data = {
        'given_url': url,
        'verified_url': None,
        'response': None
    }

    if (url.startswith(protocols[0]) or url.startswith(protocols[1])):
        # print('MSG: starts as an url.')
        url_simple_data['verified_url'] = url

    return url_simple_data

    # try to verify Url by accessing through it.
    for protocol in protocols:
        new_url = protocol+url
        print(c['orange']+uc['warning']+"MSG: Url doesn't look complete."+c['end'])
        print(c['blue']+"Validating url by completing its protocol and accessing it."+
            " Trying: "+new_url+c['end'])
        requests_library_response = get_response(new_url, 0)
        if (requests_library_response is not None):
            url_simple_data['verified_url'] = new_url
            url_simple_data['response'] = requests_library_response
            return url_simple_data

    return url_simple_data


# private, verified
def type_web_page_by_url(url_simple_data):
    ''' Chooses a type for the web_page by trying to recognize the extension
    without having access to the content, just by the URL. Defines the WebPage 
    type and instantiates it.
    Html, Pdf or None
    '''
    print(c['blue']+'MSG: Choosing type of URL.'+c['end'])
    url         = url_simple_data['verified_url']
    response    = url_simple_data['response']
    if url.endswith('.pdf'):
        return PdfWebPage(url, response)
    # most known html extensions. ['.html', '.htm', '.xhtml']
    elif word_has_extension(url, html_extensions):
        return HtmlXhtmlWebPage(url, response)
    elif word_has_extension(url, most_used_domains):
        return HtmlXhtmlWebPage(url, response)
    else:
        # might be a camouflaged PDF or any type of file. Try a generic one.
        return None

# private, verified
def type_web_page_by_response(url_simple_data, web_page_data):
    ''' Types the web page by accessing the content. Gets the content of the 
    URL(maybe it isn't an URL but a route.) and looks inside for a content 
    definition. Should return a web_page object or None.
    '''
    print(c['blue']+'MSG: Choosing type of URL by response.'+c['end'])
    url         = url_simple_data['verified_url']
    response    = url_simple_data['response']

    if (response == None): 
        response = get_response(url, 0)
    if (response == None): 
        print(c['red']+'MSG: Url looks correct but has no response. Ending.'+c['end'])
        web_page_data['warning'] = 'La URL parece correcta pero no se pudo obtener respuesta alguna.'
        return None, web_page_data # still not getting any response.

    # Checks google redirection
    #TODO: check redirection from what I've learned from Jose.
    print(c['blue']+'MSG: URL has response.'+c['end'])
    if response.text.startswith(google_string_redirection):
        print(c['blue']+'MSG: Content has google redirection.'+c['end'])
        # get true URL
        new_url = re.findall(r'navigateTo\(window.parent,window,\"(.+?)\"', 
                    response.text)[0]
        return type_web_page(new_url, web_page_data)
    else:
        web_page = None 
        print(c['blue']+'MSG: Analyzing header content.'+c['end'])
        # Look for content type(PDF or HTML)
        # https://stackoverflow.com/questions/23714383/
        # what-are-all-the-possible-values-for-http-content-type-header
        content_type_data = response.headers['content-type'].split(';')
        for content_type in content_type_data:
            type_subtype = response.headers['content-type'].split('/')
            subtype = (type_subtype[1] if len(type_subtype) == 2 else type_subtype[0])
            if('pdf' in subtype):
                web_page = PdfWebPage(url, response)
            elif('html' in subtype):
                # print("I am here")
                web_page = HtmlXhtmlWebPage(url, response)
            elif('xhtml' in subtype):
                web_page = HtmlXhtmlWebPage(url, response)
            else:
                # Might be mp3, odt or any type of file.
                # For the moment, not able to be processed.
                # this else is just for explicit explanation.
                web_page = None

        return web_page,web_page_data



# private
def type_web_page(url, web_page_data):
    """Must return a web_page object(Html or Pdf) with data loaded or a None object.
    """
    url_single_inspection_data = protocolize_and_validate_url(url)
    if(url_single_inspection_data['verified_url'] is None):
        print(c['orange']+'MSG: Unreachable content with given URL.'+c['end'])
        # web_page_data['error'] = 'No se puede acceder al contenido de la URL. No tiene el formato de una URL.'
        web_page_data['error'] = 'La URL que introdujo no es válida.'
        return None, web_page_data # Content should not be processed as a bookmarking web_page

    print(c['green']+"MSG: Verified url is: "+c['end']+url_single_inspection_data['verified_url'])
    if (url_single_inspection_data['verified_url'] != url):
        print(c['blue']+"Can't continue. "+
            "Verified URL is not the same as the one given by the user."+c['end'])
        return None, web_page_data

    # At first hand I know content type.
    web_page = type_web_page_by_url(url_single_inspection_data)
    if web_page is None:
        web_page, web_page_data = type_web_page_by_response(url_single_inspection_data, web_page_data)
    return web_page, web_page_data
            
    """
    web_page.parse_server_response_header()   
    if web_page.has_favorable_conditions():
        web_page.parse_document_content()
        # La linea comentada del millon.
        web_page.print_metadata()
    else:
        # By language knowledge; whatever the answer is; lang attribute from 
        # html element overrides server answer about language. But! let's leave it there.
        print('MSG: Url web_page does not comply favorable conditions')
    return web_page
    """


#########################################################################

#public, actual relation between background and project.
def gather_web_page_data(string_url):
    web_page_data = {
        'url':              None,
        'web_page':         None,
        'title':            None,
        'description':      None,
        'categorization':   None,
        'error':            None,
        'warning':          None,
    }

    web_page, web_page_data = type_web_page(string_url, web_page_data)

    if isinstance(web_page, PdfWebPage):
        web_page_data['warning'] = 'La URL contiene un pdf pero no puede ser procesado.'
        return web_page_data

    if web_page is None:
        # URL given can't be accessed.
        print(c['red']+"MSG: URL content can't be defined."+c['end'])
        # web_page_data['error'] = 'Contenido de URL indefinido. No se encuentra en formato HTML/XHTML.'
 
    else:
        web_page_data['url'] = web_page.get_url()
        web_page_data['web_page'] = web_page
        
        try: 
            web_page.parse_server_response_header()
            web_page.parse_document_content()
            '''
            if web_page.has_favorable_conditions():
                print(c['green']+'web page has favorable conditions'+c['end'])
                web_page.parse_document_content()
            else:
                print(c['red']+'web page has NOT favorable conditions'+c['end'])
            # web_page.parse_document_content()
            '''
        except Exception as error:
            print(c['red']+"Couldn't get response of validated url."+c['end'])
            web_page_data['warning'] = 'La URL es válida sin embargo no se puede acceder a su contenido.'

            print(error)
        else:
            # if not web_page.has_response(): web_page.get_response()
            title_contents = web_page.get_title_contents()
            if len(title_contents)>0: 
                web_page_data['title'] = title_contents[0]

            if isinstance(web_page, HtmlXhtmlWebPage):
                additional_contents = web_page.get_additional_contents()
                if len(additional_contents)>0:
                    web_page_data['description'] = additional_contents

                categorization_contents = web_page.get_categorization_contents()
                if len(categorization_contents)>0:
                    web_page_data['categorization'] = categorization_contents

    # pprint(web_page_data)
    return web_page_data

# public
def categorize_web_page_data(web_page_data):
    all_texts = []
    web_page_categories = None

    if web_page_data['title']:  all_texts.append(web_page_data['title'])
    if web_page_data['description']:  all_texts = all_texts+web_page_data['description']
    if web_page_data['categorization']:  all_texts = all_texts+web_page_data['categorization']

    if len(all_texts)==0:
        print(c['red']+'Cannot categorize text. Missing texts.'+c['end'])
        web_page_data['warning'] = 'No se pudo categorizar la página web. Metadatos insuficientes.'

    else:
        # web_page_categories = [[]]
        # a set of texts, like ['un texto', 'otro más largo', 'un, último, texto.']
        web_page_categories = categorize_text(all_texts)

    return web_page_categories


# deprecated 
def main_connection(url):
    web_page = type_web_page(new_url_response)
    web_page.parse_server_response_header()
    web_page.parse_document_content()
    web_page.print_metadata()


#########################################################################
# Action by default: gather web page data

if __name__ == '__main__': 
    content = sys.argv
    content.pop(0)
    web_page_data = gather_web_page_data(content[0])
    pprint(web_page_data)
    categorize_web_page_data(web_page_data)
