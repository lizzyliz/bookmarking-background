from setuptools import setup

setup(name='categorization',
      version='0.1',
      description='Behind the scenes for categorization',
      url='http://github.com/RizelTane',
      author='Lizbeth Leanios',
      author_email='rizelita@gmail.com',
      license='All Rights Reserved to the author',
      packages=['categorization'],
      zip_safe=False)