import sys
from pathlib import Path
from pprint import pprint

import unittest

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent)
sys.path.append(BOOKMARKING_FOLDER)

from url_functions import gather_web_page_data

from  main import type_web_page_by_response, protocolize_and_validate_url

from urls import (status_200_with_protocol, wrong_fcyt,
                    most_known_extensions_explicit_200_status,
                    no_extensions_200_status,
                    html_extensions_explicit_wrong_status,
                    most_known_extensions_explicit_wrong_status,
                    not_well_known_extensions_explicit_200_status,
                    no_extensions_wrong_status,
                    pdf_extensions_explicit_200_status,
                    pdf_extensions_explicit_wrong_status)
from urls import google_html_redirections, google_pdf_redirections


class TestTypeWebPageByResponse(unittest.TestCase):
    """ url must look like a valid url OR must be a valid url wih a response
    object. This function is called when an url cannot be interpreted as an
    html/xhtml url or a pdf url; so, the purpose of this function is to decide
    if the url is a pdf web_page or an html/xhtml web_page by evaluating the
    server response.
    Urls evaluated MUST have the protocol definition.
    """
    def test_google_html_redirections(self):
        print('--- Test google html redirection ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in google_html_redirections:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_google_pdf_redirections(self):
        print('--- Test google pdf redirection ---')
        web_page_type_string = 'PdfWebPage'
        for url in google_pdf_redirections:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)


    def test_status_200_with_protocol(self):
        print("--- Test a 200 status page with protocol ---")
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in status_200_with_protocol:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)


    def test_wrong_fcyt(self):
        print('--- Test wrong fcyt ---')
        web_page_type_string = 'NoneType'
        for url in wrong_fcyt:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_most_known_extensions_explicit_200_status(self):
        print('--- Test most known extensions explicit 200 status ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in most_known_extensions_explicit_200_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    # this may be the most important test function.
    def test_no_extensions_200_status(self):
        print('--- Test no extensions 200 status ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in no_extensions_200_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_html_extensions_explicit_wrong_status(self):
        print('--- Test html extensions explicit wrong status ---')
        web_page_type_string = 'NoneType'
        for url in html_extensions_explicit_wrong_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)


    def test_most_known_extensions_explicit_wrong_status(self):
        print('--- Test most known extensions explicit wrong status ---')
        web_page_type_string = 'NoneType'
        for url in most_known_extensions_explicit_wrong_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_not_well_known_extensions_explicit_200_status(self):
        print('--- Test not well known extensions explicit 200 status ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in not_well_known_extensions_explicit_200_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_no_extensions_wrong_status(self):
        print('--- Test no extensions wrong status ---')
        web_page_type_string = 'NoneType'
        for url in no_extensions_wrong_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_pdf_extensions_explicit_200_status(self):
        print('--- Test pdf extensions explicit 200 status ---')
        web_page_type_string = 'PdfWebPage'
        for url in pdf_extensions_explicit_200_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_pdf_extensions_explicit_wrong_status(self):
        print('--- Test PDF extensions explicit wrong status ---')
        web_page_type_string = 'NoneType'
        for url in pdf_extensions_explicit_wrong_status:
            print(url)
            res = protocolize_and_validate_url(url)
            web_page_object = type_web_page_by_response(res)
            self.assertEqual(web_page_type_string, type(web_page_object).__name__)

if __name__ == '__main__':
    unittest.main()
