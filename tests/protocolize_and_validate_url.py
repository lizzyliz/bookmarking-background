import sys
import os
import unittest

FOLDER_NAME = 'categorization'
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/'
    +FOLDER_NAME+'/')

from  main import protocolize_and_validate_url
from urls import (status_200_with_protocol, status_300_with_protocol,
                status_400_with_protocol, status_404_with_protocol,
                status_500_with_protocol)
from urls import wrong_protocol, uncomplete_wrong_urls, wrong_fcyt
from urls import google_200_status_pages, uncomplete_urls_200_status
from urls import ip_v4, ip_v6


class TestProtocolizeAndValidateUrl(unittest.TestCase):
    """ Given string urls function must validate if url is a valid url. It
    looks for an url with a protocol definition, if protocol is missing then
    it fullfills the protocol part and tries to get the url response to
    validate it, if it still doesn't work then the url is not valid nor it has
    a response)
    res[0] corresponds to url
    res[1] corresponds to response object when url was visited.
    TODO:
        - Test string limit with a very long url.
        - Test response web page that has a very big content size.
        - Test each wrong status response.
    """

    def test_urls_with_protocols(self):
        """ Given the input, function should return same url string and no
        Response object yet, because at simple sight it is a valid url.
        """
        print('--- test urls with protocols ---')
        res = []
        protocolized = [ status_200_with_protocol, status_300_with_protocol,
                         status_400_with_protocol, status_404_with_protocol,
                         status_500_with_protocol, wrong_fcyt]
        for urls_protocolized in protocolized:
            for url in urls_protocolized:
                print(url)
                res = protocolize_and_validate_url(url)
                self.assertEqual(res[0], url)
                self.assertEqual(res[1], None)

    def test_urls_with_wrong_protocols(self):
        """ Given the input, function should complete url with the protocol besides having a wrong protocol definition, then try to get a 200 status code response but as it is a wrong url there shouldn't be a response object after all.
        """
        print('--- test urls with WRONG protocols ---')
        res = []
        for url in wrong_protocol:
            print(url)
            res = protocolize_and_validate_url(url)
            # Tried to protocolize url but no response was satisfactory.
            self.assertEqual(res[0], None)
            self.assertEqual(res[1], None)

    def test_uncomplete_urls_with_200_status(self):
        """ Given the input, function should complete url with the given
        protocols, get the url response with a 200 status.
        """
        print('--- test uncomplete urls with 200 status ---')
        res = []
        uncomplete_200 = [ uncomplete_urls_200_status, google_200_status_pages]
        for uncomplete_urls in uncomplete_200:
            for url in uncomplete_urls:
                print(url)
                res = protocolize_and_validate_url(url)
                self.assertEqual(res[1].status_code, 200)

    def test_uncomplete_urls_with_wrong_name_or_service(self):
        """Given the input, function should complete url with the given
        protocols, try to get a response but the url doesn't exist or it is
        misspelled badly by the user, so no response should be returned.
        """
        print('--- test uncomplete urls with wrong name or service ---')
        res = []
        uncomplete_wrong = [ uncomplete_wrong_urls]
        for uncomplete_urls in uncomplete_wrong:
            for url in uncomplete_urls:
                print(url)
                res = protocolize_and_validate_url(url)
                self.assertEqual(res[0], None)
                self.assertEqual(res[1], None)

    def test_ip_v4(self):
        print('--- Test ip v4 ---')
        res = []
        for url in ip_v4:
            print(url)
            res = protocolize_and_validate_url(url)
            self.assertEqual(res[1].status_code, 200)

    def test_ip_v6(self):
        print('--- Test ip v6 ---')
        res = []
        for url in ip_v6:
            print(url)
            res = protocolize_and_validate_url(url)
            self.assertEqual(res[1], None)


if __name__ == '__main__':
    unittest.main()
