import sys
from pathlib import Path
from pprint import pprint

import unittest

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent)
sys.path.append(BOOKMARKING_FOLDER)

from url_functions import gather_web_page_data
from data.urls_with_title import urls_with_title
from data.urls_with_title_categorization import urls_with_title_and_categorization
from data.urls_with_title_description import urls_with_title_and_description
from data.urls_with_title_description_categorization import urls_with_title_description_and_categorization
from data.urls_no_data import urls_with_no_data
from data.urls_status_not_200 import urls_status_not_200
from data.urls_wrong_protocol import urls_wrong_protocol
from data.urls_wrong_urls import urls_wrong_urls
from data.urls_google import urls_google
from data.urls_uncomplete import urls_uncomplete
from data.urls_encoding_error import urls_encoding_error
from data.urls_with_more_than_one_data_field import more_than_one_data_field

class TestGatherWebPageData(unittest.TestCase):
    """
    # ALERT!!!
    Tests rely on everyday's web page content. Results may vary in time.
    """
    def __content_assertion(self, expected, obtained, url_verified=True):
        if url_verified:
            self.assertEqual(expected['url'], obtained['url'])
        else:
            self.assertEqual(None, obtained['url'])

        self.assertEqual(expected['title'], obtained['title'])
        self.assertEqual(expected['description'], obtained['description'])
        self.assertEqual(expected['categorization'], obtained['categorization'])


    def test_status_200_with_no_data(self):
        print('--- Test data gathering for status 200 with NO DATA ---')
        for hashed_expected_data in urls_with_no_data:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data)

    def test_status_200_with_title(self):
        print('--- Test data gathering for status 200 with TITLE ---')
        for hashed_expected_data in urls_with_title:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data)

    def test_status_200_with_title_categorization(self):
        print('--- Test data gathering for status 200 with TITLE and CATEGORIZATION ---')
        for hashed_expected_data in urls_with_title_and_categorization:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data)

    def test_status_200_with_title_description(self):
        print('--- Test data gathering for status 200 with TITLE and DESCRIPTION ---')
        for hashed_expected_data in urls_with_title_and_description:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data)

    def test_status_200_with_title_description_categorization(self):
        print('--- Test data gathering for status 200 with TITLE, DESCRIPTION and CATEGORIZATION---')
        for hashed_expected_data in urls_with_title_description_and_categorization:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data)

    def test_status_not_200(self):
        print('--- Test data gathering for status DIFFERENT as 200 ---')
        for hashed_expected_data in urls_status_not_200:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data, False)

    def test_wrong_protocol(self):
        print('--- Test data gathering for urls with WRONG PROTOCOL ---')
        for hashed_expected_data in urls_wrong_protocol:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data, False)

    def test_wrong_urls(self):
        print('--- Test data gathering for urls with WRONG URLS ---')
        for hashed_expected_data in urls_wrong_urls:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data, False)

    # even though url is corrected by the program, won't continue because it's not 
    # the content given by the user.
    def test_google_urls(self):
        print('--- Test data gathering for uncomplete google URLS ---')
        for hashed_expected_data in urls_google:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data, False)

    def test_uncomplete_urls(self):
        print('--- Test data gathering for uncomplete URLS ---')
        for hashed_expected_data in urls_uncomplete:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data, False)
    
    def test_encoding_error_urls(self):
        print('--- Test data gathering for URLS with ENCODING error ---')
        for hashed_expected_data in urls_encoding_error:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data)

    def test_more_than_one_data(self):
        print('--- Test data gathering for URLS with MORE THAN ONE DATA in field ---')
        for hashed_expected_data in more_than_one_data_field:
            web_page_data = gather_web_page_data(hashed_expected_data['url'])
            self.__content_assertion(hashed_expected_data, web_page_data)


if __name__ == '__main__':
    unittest.main()