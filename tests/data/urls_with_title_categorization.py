#################################################
# 200 status URLs with title and categorization #
#################################################
urls_with_title_and_categorization = [
    {
    'url': 'https://docs.djangoproject.com/es/1.11/topics/forms/modelforms/#specifying-widgets-to-use-in-the-form-with-widgets',
    'title':    'Creating forms from models | Django documentation | Django',
    'categorization':   ["Python, Django, framework, open-source"],
    'description':      None,
    },
    {
    'url':      'http://fcyt.umss.edu.bo',
    'title':    'Facultad de Ciencias y Tecnología - (Cochabamba - Bolivia)',
    'categorization':   ['Facultad de Ciencias y tecnología, Admisión, Pregrado, Centros de investigación, Noticias, Avisos'],
    'description':      None,
    },
]