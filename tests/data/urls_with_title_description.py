##############################################
# 200 status URLs with title and description #
##############################################
urls_with_title_and_description = [
    {
    'url':      'https://www.facebook.com',
    'title':   'Facebook - Inicia sesión o regístrate',
    'description': ["Crea una cuenta o inicia sesión en Facebook. Conéctate con amigos, familiares y otras personas que conozcas. Comparte fotos y videos, envía mensajes y..."],
    'categorization':   None,
    },
    {
    'url':      'https://www.google.com/about/',
    'title':    'Our latest | Google',
    'description': ['Get the latest news, updates, and happenings at Google. '
                 'Learn about Googleâ\x80\x99s core values and company '
                 'philosophy.'],
    'categorization':   None,
    },
    {
    'url':      'https://twitter.com/RizelTane',
    'title':    'Rizel Tane (@RizelTane) | Twitter',
    'description': ['Los Tweets más recientes de Rizel Tane (@RizelTane). '
                 '#programmer with #girlpowers. Life, and simplicity lover. '
                 'Married to @sniferl4bs but not to a programming language. '
                 '#adrenaline addict. Quite like you, I bet. 127.0.0.1' ],
    'categorization':   None,
    },
    {
    'url': 'http://reader.digitalbooks.pro/book/preview/41061/capitulo_2.xhtml',
    'title': 'Introducción a Apache Spark -  Capítulo II. Descargar y empezar con Apache Spark',
    'description': ["Digital Cloud Reader allows you to read books easily. It's "
                'plenty of funcionalities for readers '],
    'categorization': None,
    },
    {
    'url': 'https://github.com/python/pythonineducation.org',
    'title': 'GitHub - python/pythonineducation.org: The pythonineducation.org '
              'website',
    'description': ['The pythonineducation.org website. Contribute to '
                     'python/pythonineducation.org development by creating an '
                     'account on GitHub.'],
    'categorization': None,
    },
    {
    'url': 'https://www.ciberdvd.com/Flipop.net',
    'title': 'Flipop.net | Películas Completas Online Gratis | CiberDVD.com, '
             'peliculas online',
    'description': ['Flipop.net | Películas Completas Online Gratis.-\xa0'
                    'Flipop.net es un sitio para ver películas totalmente gratis '
                    'con la mejor calidad y los mejores estrenos-Ver Películas '
                    'Online Gratis Full HD Español y Latino, Ver Peliculas Online '
                    '100% gratis'],
    'categorization': None,
    },
    {
    'url': 'https://builtwith.com/aegon.de',
    'title': '\raegon.de Technology Profile',
    'description': ['Web technologies aegon.de is using on their website.'],
    'categorization': None,
    },
    {
    'url': 'https://medium.freecodecamp.org/hard-coding-concepts-explained-with-simple-real-life-analogies-280635e98e37',
    'title': 'Hard Coding Concepts Explained with Simple Real-life Analogies',
    'description': ['I love thinking about coding concepts by comparing them to '
                    'familiar things we know in life. There are so many analogies '
                    'out there about coding concepts. Some of them are good while '
                    'others are…'],
    'categorization': None,
    },
    {
    'url': 'https://www.wired.com/2011/08/how-to-add-html5-microdata-to-your-website/',
    'title': 'How to Add HTML5 Microdata to Your Website | WIRED',
    'description': ['The Opera Dev blog has posted a nice overview of HTML5 '
                    'microdata. Webmonkey took a look at microdata last year, '
                    'showing you how to extend HTML by adding custom vocabularies '
                    'to your pages. Since then microdata has moved from barely '
                    'known oddity to mainstream tool, thanks in part to '
                    'schema.org. Schema.org is a partnership between…'],
    'categorization': None,
    },
    {
    'url': 'https://github.com/lawrencewoodman/mida/wiki/Sites-Using-Microdata',
    'title': 'Sites Using Microdata · lawrencewoodman/mida Wiki · GitHub',
    'description': ['Find the most qualified people in the most unexpected '
                    'places: Hire remote! We Work Remotely is the best place to '
                    "find and list remote jobs that aren't restricted by commutes "
                    'or a particular geographic area. Browse thousands of remote '
                    'work jobs today.'],
    'categorization': None,
    },
    {
    'url':      'https://weworkremotely.com',
    'title':     'Remote Jobs: Design, Programming, Rails, Executive, Marketing, Copywriting, and more.',
    'description':      None,
    'categorization':   None,
    },
]