#################################################
# 200 status URLs with title and categorization #
#################################################
urls_with_title_description_and_categorization = [
    {
    'url': 'https://ok.ru/miuz.ru',
    'title': 'Московский ювелирный завод | OK.RU',
    'description': ['Группа Московский ювелирный завод в Одноклассниках. '
                     'Официальная группа Московского ювелирного завода\n'
                     '\n'
                     'Бриллианты меняют всё!'],
    'categorization': ['опросы, украшения, новости, бриллиант, аксессуары'],
    },
    {
    # <meta http-equiv="content-language" content="zh">
    # <meta charset="utf-8">
    # Devuelve el contenido en inglés cuando uso bot. :/
    'url': 'http://global.gmarket.co.kr/Home/Main',
    'title': 'Gmarket-Korean No.1 Shopping Site, Hottest, Trendy, Lowest Price, '
              'Worldwide shipping available',
    'description': ['Internet Shopping Mall, Auction, Bargaining, Discount '
                     'Coupon, Event, Gstamp, Gift certificate'],
    'categorization': ['Internet Shopping Mall, Auction, Bargaining, Discount '
                        'Coupon, Event, Gstamp, Gift certificate'],
    },
    {
    'url': 'http://mixi.jp/search_community.pl',
    'title': 'コミュニティ | mixi',
    'description': ['mixiコミュニティは、同じ趣味や関心ごとを持つ人たちが集まる場所。大好きなアーティストの話題やゲームの攻略情報、恋愛や育児の相談まで。270万以上あるコミュニティの中からあなたの「気になること」が見つかります。'],
    'categorization': ['mixi,ミクシィ,コミュニティ,趣味,話題,情報,相談,掲示板'],
    },
    {
    'url': 'http://whois.kisa.or.kr/kor/main.jsp',
    'title': 'KISA 한국인터넷진흥원',
    'description': ['한국인터넷진흥원 사이트에 오신것을 환영합니다.본 페이지는 WCAG2.0,KWCAG2.0 및 미래창조과학부의 '
                    '정보시스템의 구축 운영 기술 지침을 준수하여 제작하였습니다.'],
    'categorization': ['한국인터넷진흥원,온라인교육기관 KISA'],
    },
    {
    # La siguiente url indica que el char-set de la página es: euc-kr, 
    # además especifica el lenguaje en la etiqueta html como ko-KR.
    # HTML-4
    'url': 'çhttp://www.gmarket.co.kr/',
    'title': 'G마켓 - 쇼핑을 바꾸는 쇼핑',
    'description': ['인터넷쇼핑, 오픈마켓, 패션/뷰티, 디지털, 식품/유아, 스포츠/자동차, 생활용품, 도서/DVD, '
                     '여행/항공권, e쿠폰/티켓, 만화/게임, 글로벌쇼핑, 베스트, 할인쿠폰, 동영상, 이벤트'],
    'categorization': ['경매,할인쿠폰,베스트셀러,공동구매,컴퓨터/핸드폰,에어컨/TV/디카,MP3/게임,패션/명품/브랜드,여성의류/속옷,남성의류/정장/빅사이즈,분유/기저귀/식품/생리대/임부복,유아동/장난감,쌀/과일/한우/생선,건강식품/음료,화장품/자동차,스포츠/다이어트,골프/등산/스키/낚시,운동화,네비게이션,리빙/침구/인테리어,애완/성인/공구,꽃배달,도서/여행/항공권,만화'],
    },


]