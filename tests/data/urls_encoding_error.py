############################################
#  encoding error #
############################################
urls_encoding_error = [
    {
    # La url provista en este caso indica como encoding a utf-8, 
    # quizas porque la especificación debiera ser con char-set 
    # o bien porque no indique el lenguaje, no se indica mas 
    # allá que el idioma sea chino como la url de procedencia.
    # XHTML-1.0 : punycode.
    # error: al parsear con HTMLParser
    # Unicode strings with encoding declaration are not supported. 
    # Please use bytes input or XML fragments without declaration.
    'url': 'http://xn--nw2a.xn--j6w193g/', 
    'categorization': None,
    'description': None,
    'title': None,
    },
]