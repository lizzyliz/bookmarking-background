#############################
# wrong protocol definition #
#############################

urls_wrong_protocol = [
    {
    'url':      'http://62.118.138.117/',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http://23.94.10.101/',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'https://www.google.com/abouts/',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http://119.195.41.200:49152/',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http://194.209.229.111:80/',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
]