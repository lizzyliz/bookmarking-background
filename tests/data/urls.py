########################
# rightly protocolized #
########################
status_200_with_protocol = [
	'http://www.google.com',
	'http://www.google.com.bo',
    'https://www.facebook.com',
    'http://fcyt.umss.edu.bo',
    'http://www.cs.umss.edu.bo',
    'https://weworkremotely.com',
    'http://www.sniferl4bs.com',
    'https://www.google.com/about/',
]
status_300_with_protocol = [
    'http://62.118.138.117/',
]
status_400_with_protocol = [
    'http://23.94.10.101/',
]
status_404_with_protocol = [
    'https://www.google.com/abouts/',
]
status_500_with_protocol = [
    'http://119.195.41.200:49152/',
    'http://194.209.229.111:80/',
]

########################
# wrongly protocolized #
########################
wrong_protocol = [
    # el protocolo no debe estar en mayúscula.
    'HTTP://www.GOOGLE.com',
    'http:/www.google.com',
	'HTTP:www.google.com',
	'htp://www.google.com',
	'htps://www.google.com',
]


##############
# wrong urls #
##############
wrong_fcyt = [
	'http://fcyt.umss',
	'http://fcyt.umss.edu',
]
wrong_google = [
    'http:www.google.com.bo',
    'http:/www.google.com.bo',
]


################
# google       #
################
google_200_status_pages = [
	'www.google.com.',
	'google.co',
	'gogle.com',
	'google.com',
	'www.google.com',
	'www.google.com.bo',
	'ww.google.com.bo',
]

uncomplete_urls_200_status = [
	# correct most known google urls.
	'google.com.bo',
	'ww.google.com.bo',
	'www.google.com.bo',
	###
	'sniferl4bs.com',
	'ww.sniferl4bs.com',
	'www.sniferl4bs.com',
	###
	'fcyt.umss.edu.bo',
	'facebook.com',
	'FACEbook.com',
	'weworkremotely.com',
    'umss.com',
]

uncomplete_wrong_urls = [
	'mi direccion ip',
	'fcyt',
	'fcyt.umss',
	'fcyt.umss.edu',
	'sniferl4bs',
	'123454354dsfdfdfdsvjdvdkjñfdlkjgsdfasdwe',
	'mi_pagina_web_que_no_existe.com',
]
################
# ips          #
################
ip_v4 = [
	# resolves in the right way
	'172.217.6.228', # google.com
	'167.157.27.7', # www.fcyt.umss.edu.bo
]

ip_v6 = [
	# cannot be resolved
	'2800:3f0:4001:815::2003', # www.google.com.bo
]

#########################################
#########################################
# urls with different domain extensions #
#########################################
#########################################
html_extensions_explicit_200_status = [
    #TODO: Add urls with htm extension.
    # python documentation contents
    'https://docs.python.org/3/contents.html',
    # Introducción a Apache Spark - Capítulo II.
    'http://reader.digitalbooks.pro/book/preview/41061/capitulo_2.xhtml',
    # Centro de Aguas de Saneamiento Ambiental - CASA – UMSS
    # casa.fcyt inestable.
    'http://casa.fcyt.umss.edu.bo/servicios.html',
]

most_known_extensions_explicit_200_status = [
    # cs with charset iso:
    'http://www.google.com',
    'https://ok.ru/miuz.ru',
    'https://github.com/python/pythonineducation.org',
    'https://www.ciberdvd.com/Flipop.net',
    'https://builtwith.com/aegon.de',
    'http://mixi.jp/search_community.pl', #error
    'http://www.cs.umss.edu.bo/',
]

# SHOULD NOT PASS type web_page function.
no_extensions_200_status = [
    # free code camp
    """https://medium.freecodecamp.org/hard-coding-concepts-explained-with-simple-real-life-analogies-280635e98e37""",
    # global gmarket korea
    'http://global.gmarket.co.kr/Home/Main',
    # punycode:
    'http://xn--nw2a.xn--j6w193g/', #encoding error message
    # microdata and linked data:
    'https://www.wired.com/2011/08/how-to-add-html5-microdata-to-your-website/'
    ,
    # a github site with a list of pages that use microdata:
    'https://github.com/lawrencewoodman/mida/wiki/Sites-Using-Microdata',

]
##################################################

# well defined html urls but with wrong status
# contradictory: they should pass protocolize and validate url, so as type
# web_page.
html_extensions_explicit_wrong_status = [
    'http://google.com/abouts.html',
]
# contradictory: they should pass protocolize and validate url, so as type
# web_page.
most_known_extensions_explicit_wrong_status = [
    'http://burt.economicjob.com',
    'http://nytechteam.dmarc.lga1.atlanticmetro.net',
]

# contradictory: they should pass protocolize and validate url, BUT NOT type
# web_page.
not_well_known_extensions_explicit_200_status = [
    # kisa korea:
    'http://whois.kisa.or.kr/kor/main.jsp',
    # gmarket korea:
    'http://www.gmarket.co.kr/',
]
# passes protocolize and validate url, BUT NOT type web_page.
no_extensions_wrong_status = [
    'http://google.com/abouts',
]

############### from now on, mostly not tested #####################

########
# pdfs #
########
# TODO: probar pdf encubierto en html: '''https://github.com/ft-interactive/chart-doctor/blob/master/visual-vocabulary/Visual-vocabulary.pdf'''.

pdf_extensions_explicit_complete_200_status = [
    'http://docs.python.org.ar/tutorial/pdfs/TutorialPython3.pdf',
    '''http://bibing.us.es/proyectos/abreproy/12051/fichero/libros%252Flibro-django.pdf''',
]


pdf_extensions_explicit_uncomplete_200_status = [
    'docs.python.org.ar/tutorial/pdfs/TutorialPython3.pdf',
    '''bibing.us.es/proyectos/abreproy/12051/fichero/libros%252Flibro-django.pdf''',
]


pdf_extensions_explicit_wrong_status = [
    # 404, lower case mistake.
    'http://docs.python.org.ar/tutorial/pdfs/tutorialpython3.pdf',
    # 404, missing part un url.
    'https://media.readthedocs.org/pdf/django/django.pdf',
]

##TODO: probar con pruebas interesantes que fueron encontradas después.
### pruebas interesantes ###
localhost = 'localhost'
# probar páginas con black seo.


# no encuentro casos.
# TODO: buscar casos para pdfs sin extensión explícita.
# pdf_no_extension_200_status = []

# TODO: Add tests to control different types of files in redirection, not only html or pdf, but docx for example.

google_html_redirections = [
    # html
    # your first machine learning project in python
    """https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiR0-aW14nYAhUDPZAKHe6TBwwQFggpMAA&url=https%3A%2F%2Fmachinelearningmastery.com%2Fmachine-learning-in-python-step-by-step%2F&usg=AOvVaw3NxGez43PLp8kn_Swphtgj""",
    # 7 steps to mastering machine learning with python
    """https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&uact=8&ved=0ahUKEwiR0-aW14nYAhUDPZAKHe6TBwwQFghFMAQ&url=https%3A%2F%2Fwww.kdnuggets.com%2F2015%2F11%2Fseven-steps-machine-learning-python.html&usg=AOvVaw3oQUjQ1bcfopIP-rMihxMd""",
]

google_pdf_redirections = [
    """https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&cad=rja&uact=8&ved=0ahUKEwjhzIn124nYAhWKFpAKHZrmDwQQFghZMAg&url=https%3A%2F%2Fweb.stanford.edu%2F~rezab%2Fsparkworkshop%2Fslides%2Fxiangrui.pdf&usg=AOvVaw1MJFJcUpXBurepaZ1n6OgC""",
    """https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwibheDT3InYAhUIgpAKHQ7-BR8QFggmMAA&url=http%3A%2F%2Fwww.sersc.org%2Fjournals%2FIJHIT%2Fvol6_no1_2013%2F5.pdf&usg=AOvVaw1P9x890Q77JJS8bLZwDC5O""",
]
