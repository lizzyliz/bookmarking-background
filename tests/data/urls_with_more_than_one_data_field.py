#################################################
# 200 status URLs with title and categorization #
#################################################
more_than_one_data_field= [
    {
    'url': 'http://www.lostiempos.com/tendencias/interesante/20180907/retocochala-rico-vivir-cocha-realiza-recorrido-gastronomico',
    'title': '#RetoCochala: “Lo rico de vivir en Cocha” realiza recorrido '
          'gastronómico | Los Tiempos',
    'categorization': [', , , ,'],
    'description': ['Desayuno, platito de la mañana, almuerzo, el plato de media '
                 'tarde, merienda, cena y el plato de la noche son las siete '
                 'comidas que influencers tendrán que degustar en el reto '
                 'cochala',
                 ', , , , Desayuno, platito de la mañana, almuerzo, el plato '
                 'de media tarde, merienda, cena y el plato de la noche son '
                 'las siete comidas que influencers tendrán que degustar en el '
                 'reto cochala,']
    },
]