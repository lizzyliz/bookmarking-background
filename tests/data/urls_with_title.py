############################################
#  200 status URLs with only title as data #
############################################
urls_with_title = [
    # español
    {
    'url':      'http://www.google.com',
    'title':    'Google',
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http://www.google.com.bo',
    'title':    'Google',
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http://www.cs.umss.edu.bo',
    'title':    'Carreras de Informática y Sistemas --- UMSS',
    'description':      None,
    'categorization':   None,
    },
    # inglés
    {
    # otro.
    # página de wikipedia sobre HTML en árabe.
    'url': 'https://ar.wikipedia.org/wiki/%D9%84%D8%BA%D8%A9_%D8%AA%D8%B1%D9%85%D9%8A%D8%B2_%D8%A7%D9%84%D9%86%D8%B5_%D8%A7%D9%84%D9%81%D8%A7%D8%A6%D9%82',
    'title': 'لغة ترميز النص الفائق - ويكيبيديا، الموسوعة الحرة',
    'description':      None,
    'categorization':   None,
    },
    #############################
    {
    'url': 'https://docs.python.org/3/contents.html',
    'title': 'Python Documentation contents — Python 3.7.3 documentation',
    'description': None,
    'categorization': None,
    },
    {
    'url': 'http://www.cs.umss.edu.bo/',
    'title': 'Carreras de Informática y Sistemas --- UMSS',
    'categorization': None,
    'description': None,
    },
    {
    'url':      'http://www.sniferl4bs.com',
    'title':    "Snifer@L4b's",
    'description':      None,
    'categorization':   None,
    },
]