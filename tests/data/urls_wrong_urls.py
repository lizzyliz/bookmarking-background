#############################
# wrong protocol definition #
#############################

urls_wrong_urls = [
    {
    'url':      'http://fcyt.umss',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http://fcyt.umss.edu',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http:www.google.com.bo',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
    {
    'url':      'http:/www.google.com.bo',
    'title':    None,
    'description':      None,
    'categorization':   None,
    },
]