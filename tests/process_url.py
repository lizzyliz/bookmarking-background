import os
import sys
import unittest

FOLDER_NAME = 'categorization'
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/'
    +FOLDER_NAME+'/')

from  main import process_url
from urls import *  # all maybe.

class TestProcessUrl(unittest.TestCase):
    """ A function to sequence: protocolize and validate url, type web_page and type web_page by response. It should return a web_page object, but if it can't determine what type of web_page is then a None object will be returned.
    It is better to run this test function by function.
    """

    def test_status_200_with_protocol(self):
        print('--- Test status 200 with protocol. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in status_200_with_protocol:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_wrong_protocol(self):
        print('--- Test wrong protocol. ---')
        web_page_type_string = 'NoneType'
        for url in wrong_protocol:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_wrong_fcyt(self):
        print('--- Test wrong fcyt urls. ---')
        web_page_type_string = 'NoneType'
        for url in wrong_fcyt:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_wrong_google(self):
        print('--- Test wrong google urls. ---')
        web_page_type_string = 'NoneType'
        for url in wrong_google:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_google_200_status_pages(self):
        print('--- Test google 200 status pages. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in google_200_status_pages:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_uncomplete_urls_200_status(self):
        print('--- Test uncomplete urls 200 status. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in uncomplete_urls_200_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_uncomplete_wrong_urls(self):
        print('--- Test uncomplete wrong urls. ---')
        web_page_type_string = 'NoneType'
        for url in uncomplete_wrong_urls:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_ip_v6(self):
        print('--- Test ip v6 urls. ---')
        web_page_type_string = 'NoneType'
        for url in ip_v6:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_ip_v4(self):
        print('--- Test ip v4 urls. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in ip_v4:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_html_extensions_explicit_200_status(self):
        print('--- Test html extensions explicit 200 status. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in html_extensions_explicit_200_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_most_known_extensions_explicit_200_status(self):
        print('--- Test most known extensions explicit 200 status. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in most_known_extensions_explicit_200_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_no_extensions_200_status(self):
        print('--- Test no extensions 200 status. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in no_extensions_200_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_html_extensions_explicit_wrong_status(self):
        """ By having the html extension and a protocol, it is treated as an HtmlXhtmlWebPage, but not processed.
        """
        print('--- Test html extensions explicit wrong status. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in html_extensions_explicit_wrong_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_most_known_extensions_explicit_wrong_status(self):
        """ By having the .com or .net or .well_known_domain extension and a protocol, it is treated as an HtmlXhtmlWebPage, but not processed.
        """
        print('--- Test most known extensions explicit wrong status. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in most_known_extensions_explicit_wrong_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_not_well_known_extensions_explicit_200_status(self):
        print('--- Test not well known extensions explicit 200 status. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in not_well_known_extensions_explicit_200_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_no_extensions_wrong_status(self):
        print('--- Test no extensions wrong status. ---')
        web_page_type_string = 'NoneType'
        for url in no_extensions_wrong_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_pdf_extensions_explicit_200_status(self):
        print('--- Test PDF extensions explicit 200 status. ---')
        web_page_type_string = 'PdfWebPage'
        for url in pdf_extensions_explicit_200_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_pdf_extensions_explicit_wrong_status(self):
        """ urls will be recognized as pdf files cause of the extension, but they won't be processed.
        """
        print('--- Test PDF extensions explicit wrong status. ---')
        web_page_type_string = 'PdfWebPage'
        for url in pdf_extensions_explicit_wrong_status:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_google_html_redirections(self):
        print('--- Test google html redirections. ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for url in google_html_redirections:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)

    def test_google_pdf_redirections(self):
        print('--- Test google PDF redirections. ---')
        web_page_type_string = 'PdfWebPage'
        for url in google_pdf_redirections:
            print(url)
            web_page = process_url(url)
            self.assertEqual(web_page_type_string, type(web_page).__name__)


if __name__ == '__main__':
    unittest.main()
