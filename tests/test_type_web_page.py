import sys
from pathlib import Path
from pprint import pprint

import unittest

BOOKMARKING_FOLDER = str(Path(__file__).resolve().parent.parent)
sys.path.append(BOOKMARKING_FOLDER)

from url_functions import type_web_page
from data.urls import (
    status_200_with_protocol,
    status_300_with_protocol,
    status_400_with_protocol,
    status_404_with_protocol,
    status_500_with_protocol,
    wrong_protocol,
    wrong_fcyt,
    wrong_google,
    google_200_status_pages,
    uncomplete_urls_200_status,
    uncomplete_wrong_urls,
    ip_v4,
    ip_v6,
    html_extensions_explicit_200_status,
    most_known_extensions_explicit_200_status,
    no_extensions_200_status,
    html_extensions_explicit_wrong_status,
    most_known_extensions_explicit_wrong_status,
    not_well_known_extensions_explicit_200_status,
    no_extensions_wrong_status,
    pdf_extensions_explicit_200_status,
    pdf_extensions_explicit_wrong_status,
    # pdf_no_extension_200_status
    google_html_redirections,
    google_pdf_redirections
)


class TestTypeWebPage(unittest.TestCase):
    """ tests wether a string that represents an URL can be typed
    as a WebPage typed class (HtmlXhtmlWebPage, PdfWebPage or None).
    """

    def test_all_HtmlXhtml(self):
        print('--- Test all HtmlXhtml web pages ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        all_htmlxhtml_urls = [
            status_200_with_protocol,
            html_extensions_explicit_200_status,
            most_known_extensions_explicit_200_status,
            no_extensions_200_status,
            ############################
            ### has no content to access but looks like a complete url
            ############################
            html_extensions_explicit_wrong_status,
            most_known_extensions_explicit_wrong_status,
            not_well_known_extensions_explicit_200_status,
        ]
        for url_set in all_htmlxhtml_urls:
            for url in url_set:
                print(url)
                web_page_object = type_web_page(url)
                self.assertEqual(web_page_type_string, type(web_page_object).__name__)

    def test_all_None(self):
        print('--- Test all None. ---')
        web_page_type_string = 'NoneType'

        all_wrong_urls = [
            status_300_with_protocol,
            status_400_with_protocol,
            status_404_with_protocol,
            status_500_with_protocol,
            wrong_protocol,
            wrong_fcyt,
            wrong_google,
            ###############################
            ### uncomplete. no protocol ###
            ###############################
            google_200_status_pages, 
            uncomplete_urls_200_status,
            uncomplete_wrong_urls,
            ip_v4,
            ip_v6,
            ## no extension wrong status ##
            no_extensions_wrong_status,
            ## pdf ##
            pdf_extensions_explicit_uncomplete_200_status,
            pdf_extensions_explicit_wrong_status,
        ]

        for url_set in all_wrong_urls:
            for url in url_set:
                print(url)
                web_page_object = type_web_page(url)
                self.assertEqual(web_page_type_string, type(web_page_object).__name__)


    def test_all_Pdf(self):
        print('--- Test all Pdf web pages ---')
        web_page_type_string = 'PdfWebPage'

        all_pdf_urls = [
            pdf_extensions_explicit_complete_200_status,
        ]
    
        for url_set in all_wrong_urls:
            for url in url_set:
                print(url)
                web_page_object = type_web_page(url)
                self.assertEqual(web_page_type_string, type(web_page_object).__name__)


if __name__ == '__main__':
    unittest.main()
