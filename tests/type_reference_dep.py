import sys
import os
import unittest

FOLDER_NAME = 'categorization'
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'/'
    +FOLDER_NAME+'/')

from main import type_web_page, protocolize_and_validate_url

from urls import ip_v4

from urls import html_extensions_explicit_200_status
from urls import most_known_extensions_explicit_200_status
from urls import no_extensions_200_status
from urls import (html_extensions_explicit_wrong_status,
                    most_known_extensions_explicit_wrong_status,
                    not_well_known_extensions_explicit_200_status,
                    no_extensions_wrong_status)
from urls import (pdf_extensions_explicit_200_status,
                    pdf_extensions_explicit_wrong_status)

class TestTypeWebPage(unittest.TestCase):
    """ By knowing that the url has url form OR by knowing that the url
    returns a response with 200 status, in colaboration with the protocolize
    and validate url function, this function will try to decide if the url is
    a web_page to a pdf or to an html/xhtml page.
    """

    def test_urls_html_explicit_with_200_status(self):
        """ url has html or xhtml extension definition in url.
        """
        print('--- test urls explicit with 200 status ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for html_url in html_extensions_explicit_200_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_urls_most_known_extensions_explicit_with_200_status(self):
        """ url has one of the most known extensions: .com, .org, .net, etc
        """
        print('--- test most known extensions explicit with 200 status ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for html_url in most_known_extensions_explicit_200_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_urls_no_extension_with_200_status(self):
        """ url has no extension so it doesn't know which type web_page
        should be at simple sight.
        """
        print('--- test urls with no extension and 200 status ---')
        web_page_type_string = 'NoneType'
        for html_url in no_extensions_200_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_urls_html_extensions_explicit_wrong_status(self):
        """ url has html/xhtml extension so it should evaluate to html/xhtml.
        """
        print('--- test urls with html extension but wrong status ---')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for html_url in html_extensions_explicit_wrong_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_urls_most_known_extensions_explicit_wrong_status(self):
        """ url has one of the most known extensions so it should evaluate to
        html/xhtml.
        """
        print('''--- test urls with most known extensions explicit but wrong
            status ---''')
        web_page_type_string = 'HtmlXhtmlWebPage'
        for html_url in most_known_extensions_explicit_wrong_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_urls_not_well_known_extensions_explicit_200_status(self):
        """ url has one of the least known extensions so it shouldn't pass.
        """
        print('''--- test urls with not well known extensions explicit but a
            200 status ---''')
        web_page_type_string = 'NoneType'
        for html_url in not_well_known_extensions_explicit_200_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_no_extensions_wrong_status(self):
        """ url has no extension defined so it shouldn't pass.
        """
        print('--- test urls with no extension and a wrong status ---')
        web_page_type_string = 'NoneType'
        for html_url in no_extensions_wrong_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_ipv4(self):
        """ ipv4 has no extension defined so it shouldn't pass.
        """
        print('--- test ipv4 urls ---')
        web_page_type_string = 'NoneType'
        for html_url in ip_v4:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_pdf_urls_200_status(self):
        """ They should be recognized as pdf web_pages.
        """
        print('--- test pdf urls with 200 status ---')
        web_page_type_string = 'PdfWebPage'
        for html_url in pdf_extensions_explicit_200_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

    def test_pdf_urls_wrong_status(self):
        """ They should be recognized as pdf web_pages. It passes protocolize and validate url because it looks like a correct url.
        """
        print('--- test pdf urls with WRONG status ---')
        web_page_type_string = 'PdfWebPage'
        for html_url in pdf_extensions_explicit_wrong_status:
            print(html_url)
            res = protocolize_and_validate_url(html_url)
            web_page_object = type_web_page(res)
            self.assertEqual(web_page_type_string, type(web_page_object).
                __name__)

if __name__ == '__main__':
    unittest.main()
