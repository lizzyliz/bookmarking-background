#!/usr/bin/python3.6.0
# -*- coding: utf-8 -*-

import datetime
import requests
import re
import sys
from pathlib    import Path
from abc        import ABCMeta, abstractmethod

import bm_traverse_modules.server
from bm_traverse_modules.cross import lang_evaluation
from meta.metadata import Metadata
from web_constants import (userAgentsDictionary, userAgentsList, 
    number_of_tries, MAX_PAGE_LENGTH, english_definition, spanish_definition)
from console_constants import (unicode_characters as uc, colors as c)
from pprint import pprint 

bookmarking_folder = str(Path(__file__).resolve().parent.parent.parent)
sys.path.append(bookmarking_folder)
from bm_traverse_modules.text_processing import (split_by_delimiters, belongs_to_latin_spanish_alphabet)


class WebPage(metaclass=ABCMeta):
    '''WebPage will be classified in one up to three categories for the first 
    time the URL is saved. Later on for future versions, general web page data 
    may be updated, changed or deleted.
    '''
    def __init__(self, url):
        self.__tries    = 0   # private
        self._url       = url # protected
        self._title     = ''
        # encoding, length and language are only set when checked.
        self._encoding  = None
        self._length    = 0 # bytes
        self._languages =  {}
        self._languages_amount = 0

        self.in_english = None 
        self.in_spanish = None
        self.latin_spanish_alphabet = None

        try: 
            if not isinstance(self._metadata, Metadata):
                # Child must have created metadata as a metadata type..
                raise Exception('MetadataType Exception.')
        except Exception as e:
            print("ERROR: metadata must exist and it has to be a sub-instance\
                of Metadata class.")
            sys.exit(1)

    @abstractmethod
    def print_metadata():
        pass

    
    @abstractmethod
    def _parse_metadata(self, web_page_metadata):
        ''' gets self._content and returns a metadata object.
        if content is the same as before, metadata also. 
        [save(laying up) parsing action]
        '''
        pass

    @abstractmethod
    def _parse_body(self,web_page_metadata):
        pass

    @abstractmethod
    def parse_document_content(self, content_variables):
        # public method. Not a mistake.
        pass

    @abstractmethod
    def check_language_in_content(self):
        ''' Checks if language is favorable in content, By trusting and checking
        html tag in HTML/XHTML web pages, and the title's content.
        '''
        pass

    @abstractmethod
    def _fetch_title(self, new_title):
        '''
        Reads self.metadata and defines according it the web page title.
        '''
        pass

    @abstractmethod 
    def _clean_content(self):
        pass

    ###########################################
    ###    setters, getters and checkers    ###
    ###########################################
    def _set_local_encoding(self, source, value):
        '''
        sample; 'Content-Type': 'text/html; charset=utf-8'
        Do not trust in page's codification, if it can be parsed it's
        all that really matters (being able to read the content). 
        It can be improved.
        '''
        try:
            result = re.search('charset=(.+);?', value)
            # print(result.group(1))
            self._encoding = result.group(1)
        except Exception as error:
            return False
        else:
            return True 

    
    def _set_local_alphabet(self, texts_array):
        for text in texts_array:
            words = split_by_delimiters(text)
            for word in words:
                # print('word to be evaluated:'+word)
                if not(belongs_to_latin_spanish_alphabet(word)):
                    # print('guilty word:'+word)
                    self.latin_spanish_alphabet = False
                    return None

        self.latin_spanish_alphabet = True


    def _set_local_languages(self, source, value):
        results = lang_evaluation(value)
        if not any(results):
            return False
        if english_definition in results: 
            self.in_english = True
            return True
        if spanish_definition in results: 
            self.in_spanish = True
            return True
        return False

    def _set_local_length(self, source, value):
        if value == '':
            # print('MSG: length not set in header.')
            value = len(self._response.text)
        self._length = value
        return True



    def check_favorable_size(self):
        if int(self._length) > MAX_PAGE_LENGTH:
            # it exists the program
            print('MSG: '+c['red']+uc['wrong_checked']+c['end']+
                ' Not favorable, Max size reached.')
            return False
        else: 
            print ('MSG: '+c['green']+uc['right_checked']+c['end']+
                'Favorable size. Aprox: %.2f KB' % (float(self._length)/1024.0))
            return True

    def check_favorable_encoding(self):
        '''In Python 3, all strings are sequences of Unicode characters
        https://stackoverflow.com/questions/10156090/
        how-do-i-detect-if-a-file-is-encoded-using-utf-8
        '''
        if not self._encoding:
            # no encoding definition, no nothing?
            print('MSG: '+c['orange']+uc['warning']+c['end']+
                'Encoding not set because it was not defined. \
                Unicode by default.')
        else: 
            print('MSG: '+c['green']+uc['right_checked']+c['end']+
                'Encoding set in header: '+self._encoding)


    def check_favorable_language(self):
        if self.in_spanish or self.in_english or self.latin_spanish_alphabet:
            return True
        elif(self._languages_amount == 0):
            # Language not specified.
            return None
        else:
            # A language was specified, but not spanish nor english.
            return False


    def check_favorable_alphabet(self):
       return self.latin_spanish_alphabet


    ###############################
    def parse_server_response_header(self):
        ''' Obtains the most important header variables information of server 
        response. After this method is called, parse_document_content() will be 
        called so it should have been implemented.
        '''
        if not self._response: self.get_response()
        from web_constants import server_headers
        for local_name, header_variable in server_headers.items():
            function_name = '_set_local_'+local_name
            if header_variable in self._response.headers.keys():
                header_value = self._response.headers[header_variable].lower()
            else:
                # header_value must be a string.
                header_value = ''

            function = getattr(self, function_name)
            # print("Value to analize: "+header_value)
            function('header-response', header_value)

    #################################################
    # public
    def has_favorable_conditions(self):
        '''Check first size, then codification and lastly language. 
        Order is important.
        '''
        still_favorable = True
        # checking favroable conditions in header
        self.check_favorable_encoding()
        language_is_favorable = self.check_favorable_language()


        if language_is_favorable is None:
            print('MSG: '+c['orange']+uc['warning']+c['end']+
                'Language is not defined in header.')
            self.check_language_in_content()
            if self.check_favorable_language() is not True:
                still_favorable = False
                print('MSG: '+c['orange']+uc['black_flag']+c['end']+
                'Language content not favorable.')
            else:
                print('MSG: '+c['green']+uc['right_checked']+c['end']+
                    'Content was evaluated as spanish or english or has latin letters.')

        elif language_is_favorable is True: 
            print('MSG: '+c['green']+uc['right_checked']+c['end']+
                'Language set in header is spanish.')

        elif self.check_favorable_alphabet():
            print('MSG: '+c['green']+uc['right_checked']+c['end']+
                'Content seems to belong to latin spanish alphabet.')
        else:
            print('MSG: '+c['orange']+uc['warning']+c['end']+
                'Not favorable, a language was declared and it is not spanish.')
            pprint(self._languages)
            still_favorable = False
        return still_favorable

    def has_response(self):
        if self._response: return True
        else: return False

    def get_response(self):
        if not self._response:
            print(c['blue']+'MSG: Obtaining response. Working'+c['end'])
            self._response = bm_traverse_modules.server.get_response(self._url)
        else:
            print(c['blue']+'MSG: Response already set.'+c['end'])
            
        # if exception does not work
        if not self._response: print(c['red']+"MSG: Couldn't get response."+c['end'])
        else: print(c['green']+"MSG: response obtained."+c['end'])


    def get_url(self):
        return self._url

    def _get_local_languages(self):
        return self._languages

    # lastly save date and status. 
    # Don't forget to check if content for web_page is already available. 
    # It also can hace a different content in time. :|
    #version urllib2, deprecado.
    def __str__(self):
        return self.url+" "+self.title