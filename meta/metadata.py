#!/usr/bin/python3.5.0

from abc import ABCMeta, abstractmethod
from .constants import directives, meta_key_words

class Metadata(metaclass=ABCMeta):

    def __init__(self):
        # metadata_elements is a hash with MetadataElement typed elements.
        # print("this is the whole metadata to process")
        self._metadata_elements = {}


    def save_metadata_element(self, metadata_element):
        md_element_type = metadata_element.md_element_type
        if md_element_type in self._metadata_elements:
            self._metadata_elements[md_element_type].append(metadata_element)
        else:
            self._metadata_elements[md_element_type] = []
            self.save_metadata_element(metadata_element)


    def print_metadata_elements(self):
        print("=========================")
        print("SHOWING METADATA ELEMENTS")
        print("=========================")
        for md_element_type, md_elements in self._metadata_elements.items():
            print("MD ELEMENT TYPE: "+md_element_type)
            print("AMOUNT:          "+str(len(md_elements)))
            print("-------------------------")
            for md_element in md_elements: md_element.print_content()


    # get_from_list previously
    def get_metadata_element(self, text, type):
        print('A search function.')
        pass

    #@abstractmethod
    # def process_meta_element(self, single_element):
    #     pass
