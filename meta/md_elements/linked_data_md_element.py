#!/usr/bin/python3.5.0

from .md_element import MetadataElement

"""
self.element_type
self.languages
self.md_element_type
self.type
self.value
"""
class LinkedDataMetadataElement(MetadataElement):
    # def __init__(self, meta_type, json_hash_data):   
    # La llamada a esta clase es directa, no de forma dinámica.
    def __init__(self, element_type, json_hash_data, languages={}):
        self.element_type = element_type
        self.languages = languages
        self.md_element_type = 'linked_data' # Por el momento.

        if '@type' in json_hash_data: self.type = json_hash_data['@type']
        self.value = json_hash_data
        # super().__init__(meta_type, tag_name, tag_content)
