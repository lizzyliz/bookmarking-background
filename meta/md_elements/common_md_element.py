#!/usr/bin/python3.5.0

from .md_element import MetadataElement

"""
self.element_type
self.languages
self.md_element_type
self.type
self.value
"""
class CommonMetadataElement(MetadataElement):
    
    def __init__(self, element_type, attribute, value, tag, languages={}):
        self.element_type = element_type
        self.languages  = languages
        self.md_element_type = 'common' # Por el momento.

        self.type   = attribute
        self.value  = value

        