#!/usr/bin/python3.5.0

import pprint
from abc import ABCMeta, abstractmethod

class MetadataElement(metaclass=ABCMeta):
    '''
    example:
    tag_type      = 'simple'
    meta_tag_name = 'title'
    content       = 'This is the title of an html page'
    # lang          = '' solo apareceria si existe tal elemento.

    tag_type      = 'simple_vocabulary'
    namespace     = 'og'
    meta_tag_name = 'title'
    content       = 'GovPredict: Senior Data Engineer'
    lang          = ''

    tag_type      = 'simple_vocabulary'
    namespace     = 'twitter'
    meta_tag_name = 'card'
    content       = 'summary'
    lang          = ''

    tag_type      = simple_vocabulary
    namespace     = fb
    meta_tag_name = app_id
    content       = 

    tag_type      = triple_statement
    namespace     = who knows yet.
    '''

    #deprecated
    def __init2__(self, meta_type, name, content, aditional):
    # def __init__(self, meta_type, name, content):
        print('Element created.')
        self.type       = meta_type
        self.name       = name
        self.content    = content
        # # language and namespace definition.
        if aditional is not None:
            for attribute, value in aditional.items():
                if value != None: setattr(self, attribute, value)

    #deprecated
    """
    def __init__(self, meta_type, name, content):
    # def __init__(self, meta_type, name, content):
        self.type       = meta_type
        self.name       = name
        self.content    = content
    """


    def print_content(self):
        """
        self.element_type
        self.languages
        self.md_element_type
        self.type
        self.value
        """
        print("Md element type: "+ self.md_element_type +"Element type: "+ self.element_type)
        print("Type: "+self.type)
        if self.languages: print("Languages: "+self.languages)
        if isinstance(self.value, dict):
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint(self.value)
            
        else:
            # Printing value as a string.
            print("Value: "+self.value)
        print("-------------------------")





#print("if author, title, media etc. exists.")
