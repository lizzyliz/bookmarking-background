#!/usr/bin/python3.5.0

from .md_element import MetadataElement
"""
self.element_type
self.languages
self.md_element_type
self.type
self.value

self.prefix
self.term
self.datatype
self.dictionary
"""

class RdfaMetadataElement(MetadataElement):

    # meta, property, og:name, weworkremotely, <etreeElement>, en
    def __init__(self, element_type, attribute, value, tag, languages={}):
        # check if it has compact URI expressions. No importa si el vocabulario está o no definido, si tiene apariencia lo considero y luego complemento con información obtenida de la cabecera.
        # check if it looks like being splitted.
        # COMMENT: should be an else, not a try. or not¿?
        self.element_type = element_type
        self.languages = languages
        self.md_element_type = 'rdfa' # Por el momento.
        
        if(attribute == 'property'):
            self.type = attribute
            # check if property looks like a full IRI.
            # <meta property="http://purl.org/dc/terms/creator" content="Mark Birbeck" />
            if(value.startswith('http://')):
                self.prefix, self.term = self.get_iri_prefix_and_term(value)
            # <meta property="dc:creator" content="Mark Birbeck" />
            elif ':' in value:
                self.prefix, self.term = self.get_curie_prefix_and_term(value)
            # My name is <span property="name">John Doe</span> and my blog is called
            else:
                self.prefix = ''
                self.term = value

            if 'content' in tag.attrib: self.value = tag.attrib['content']
            elif 'href' in tag.attrib: self.value = tag.attrib['href']
            else: self.value = tag.text

            if 'datatype' in tag.attrib: self.datatype = tag.attrib['datatype']

        # <html xmlns="http://www.w3.org/1999/xhtml"
        # prefix="bibo: http://purl.org/ontology/bibo/
        # dc: http://purl.org/dc/terms/"  >
        elif(attribute == 'prefix'):
            self.type = attribute
            self.dictionary = {}
            words = value.split(' ')
            for prefix , url_vocabulary in zip(words[0::2], words[1::2]):
                # create rdfa element with prefix as specified.
                self.dictionary[prefix] = url_vocabulary

        elif(attribute == 'vocab'):
            self.type = attribute
            self.dictionary = {}
            self.dictionary['vocab'] = value
        else:
            # ingresó por la forma w:w
            self.type = 'namespace:prefix'
            namespace, prefix = attribute.split(':')
            self.dictionary = {}
            self.dictionary[prefix] = [namespace, value]




    def get_iri_prefix_and_term(self, text):
        # values = {}
        last_slash_position = 0
        mv_length = len(text)
        for i in range(mv_length-1, -1,-1):
            if text[i] == '/': 
                last_slash_position = i 
                break
        vocabulary = text[0:last_slash_position]
        vocabulary_term = text[last_slash_position+1:mv_length]
        # values['vocabulary'] = vocabulary
        # values['terms'] = vocabulary_term
        return [vocabulary, vocabulary_term]

    def get_curie_prefix_and_term(self, text):
        # values = {}
        words = text.split(':')
        words_length = len(words)
        vocabulary = ''
        for i in range(words_length-1): 
            vocabulary += (words[i]+':')
        # for word in words
        vocabulary = vocabulary[:-1] # remove last colon.
        vocabulary_term = words[words_length-1]
        return [vocabulary, vocabulary_term]

    def print_content(self):
        super().print_content()
        if hasattr(self,'prefix'):
            print('prefix: '+self.prefix)
        if hasattr(self,'term'):
            print('term: '+self.term)
        if hasattr(self,'datatype'):
            print('datatype: '+self.datatype)
        if hasattr(self,'dictionary'):
            print('dictionary: '+self.dictionary)


'''
        aditional_data = {}
        aditional_data['vocabulary'] = vocabulary
        aditional_data['vocabulary_term'] = vocabulary_term
        aditional_data['language'] = get_language_definition(meta_tag)
        try: 
            metadata_element = self.create_metadata_element('rdfa', vocabulary_term, meta_tag.attrib['content'], aditional_data)
            # self.metadata.save_in_list(meta_element)
            self.save_metadata_element(metadata_element)
        except:
            # There's no content attribute, skip this meta tag.     
            return
'''

'''

    # porque el namespace puede estar conformado por una url o mas de un "dominio"
    # def check_rdfa(self, text, meta_tag):
    element_class(tag_type, meta_attribute, text, tag)
'''