#!/usr/bin/python3.5.0

from .md_element import MetadataElement
"""
self.element_type
self.languages
self.md_element_type
self.type

antes:
    self.item_type_names
    self.item_type_values
    self.item_property_names
    self.item_property_values
ahora: item-> name, value
"""

class MicrodataMetadataElement(MetadataElement):


    # check_microdata basado en http://schema.org/docs/gs.html
    # el ejemplo en el apartado 3c es bastante completo.
    # revisión en base a: https://www.w3.org/TR/microdata/
    def __init__(self, element_type, attribute, value, tag, languages={}):
        # si ingresó por itemscope no es relevante, sólo confirma que hay un elemento descrito con microdata. En la cabecera no se han visto elementos anidados.
        # Item types are provided as URLs
        # content over value. Content weights more. Content over src o href.
        #itemid podria ser interesante para después, sólo para relaciones externas.
        # si ingresó por itemscope  reviso si a continuación tiene los elementos type y prop, porque en la cabecera es casi imposible ver elementos anidados. La presencia de itemscope debe hacerse en la etiqueta html.

        # crear elemento de metadata, por algo ingresó por aqui.
        # puede un microdato a la vez ser un rdf, yo digo que si. Serian dos elementos diferentes
        self.element_type    = element_type
        self.languages       = languages
        self.md_element_type = 'microdata' # Por el momento.

        self.type = attribute
        self.value = {}

        print('got into microdata')
        print("value: "+value)

        if'itemtype' == attribute:
            self.value['itemtype'] = {}
            self.value['itemtype']['names'] = tag.attrib['itemtype'].split(' ')
            # TODO: arreglar para la creación de elementos de diferentes tipos: # append????
            self.value['itemtype']['values'] = tag.text

        elif'itemprop' == attribute:
            print("tag: "+tag)
            # si existe la propiedad entonces el contenido es su valor.
            # más de una propiedad con el mismo valor, una propiedad con otro vocabulario adicional, la propiedad indica una url diferente a la general.
            self.value['itemprop'] = {}
            self.value['itemprop']['names'] = tag.attrib['itemprop'].split(' ')
            self.value['itemprop']['values'] = [] # or use hash instead for precedence importance.
            if tag.text != '': 
                self.value['itemprop']['values'].append(tag.text)

            from meta.constants import text_attributes
            for text_attribute in text_attributes:
                if text_attribute in tag.attrib:
                    self.value['itemprop']['values'].append(tag.attrib[text_attribute])
                     # TODO: arreglar para la creación de elementos de diferentes tipos:
        else:
            print('I should have never existed.')