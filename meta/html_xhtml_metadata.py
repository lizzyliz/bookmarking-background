#!/usr/bin/python3.5.0

import json
import io
import re

from .md_elements.common_md_element import CommonMetadataElement
from .md_elements.rdfa_md_element import RdfaMetadataElement
from .md_elements.microdata_md_element import MicrodataMetadataElement
from .md_elements.linked_data_md_element import LinkedDataMetadataElement

from .metadata import Metadata
from bm_traverse_modules.html_xhtml import get_language_definition
from .constants import *

from pprint import pprint

# This class is responsible for communicating with MetaDataElement.
class HtmlXhtmlMetadata(Metadata):

    # TODO: mover los check meta a clase abstracta, ellos son los que se comunican con Metadata. No porque no es genérico ni incluye a Pdf. Mas bien se construyó a MetadataElement desde la clase abstracta para hacer la comunicación.

    # def check_meta_name(self, meta_attribute, meta_value):
    def check_meta_name(self,meta_attribute,  meta_value, meta_tag):
        print(" meta name value: "+ meta_value)

        for meta_key in meta_key_words:
            if meta_value in meta_key_words[meta_key]:
                # create element
                # MetadataElementSig(meta_type, name, content, **kwargs)
                aditional_data = {}
                aditional_data['language'] = get_language_definition(meta_tag)
                try: 
                    metadata_element = self.create_metadata_element(meta_key, meta_value, meta_tag.attrib['content'], aditional_data)
                    self.save_metadata_element(metadata_element)
                except:
                    # There's no content attribute, skip this meta tag.
                    print('no content')     
                    return



    def check_charset(self,meta_attribute,  meta_value, meta_tag):
        print("skipped "+ meta_value)
        

    def is_a_useful_meta_by(tag_name, meta_tag):
        for word in meta_words:
            if meta_tag.attrib[tag_name] in word:
                return True
        return False

    #############################

    def process_etree_element(self, etree_tag):
        from lxml import etree
        # print(type(etree_tag))
        if isinstance(etree_tag, etree._Comment): return None
        tag_name = etree_tag.tag.lower()
        print(tag_name)
        languages = {}

        if 'lang' in etree_tag.attrib:
            from cross import lang_evaluation
            lang_value = etree_tag.attrib['lang']
            lang_results = lang_evaluation(lang_value)
            # pprint(lang_results)
            if (len(lang_results) > 0):
                # may have more than just one language definition.
                languages = lang_results
            else: 
                # abort processing mission.
                print("MSG: Language not considered. Definition is different from permitted.")

        if (tag_name == 'html') or (tag_name == 'head'):
            tag_type = 'general'
            try:
                # does language attribute in tag exists?
                lang_results
                # Has admited languages been recognized?
                if (len(languages) == 0):
                    print("MSG: Defined language for 'root' tags not admited.")
                    sys.exit(0)
                self.process_by_attributes(etree_tag, tag_type, languages)
            except NameError:
                # NO language definition for html tag or head tag.
                self.process_by_attributes(etree_tag, tag_type)

        elif (tag_name == 'title'):
            tag_type = 'single'
            # Tag is title, I already know which class to call.
            metadata_element    = CommonMetadataElement(tag_type, tag_name,  etree_tag.text, languages)
            self.save_metadata_element(metadata_element)
            # Find other types of metadata defined.
            self.process_by_attributes(etree_tag, tag_type, languages)

        elif (tag_name == 'meta'):
            tag_type = 'single'
            # print('processing meta tag.') 
            self.process_by_attributes(etree_tag, tag_type, languages)

        # If it is script tag it can only be automatically LinkedData, No data about microdata or rdfa in script tag. Verifying ld+json
        elif (tag_name == 'script') and ('type' in etree_tag.attrib) and ('ld+json' in etree_tag.attrib['type']):
            try: 
                meta_io = io.StringIO(etree_tag.text)
                json_object = json.load(meta_io)
                tag_type = 'single'
                metadata_element = LinkedDataMetadataElement(tag_type,  json_object, languages)
                self.save_metadata_element(metadata_element)
                
            except Exception as e:
                print('json exception')
                print(e)


        else:
            # to avoid other types like link or script or something else which was not considered, just pass.
            # print('::omitido.')
            return None

        # not useful now: meta_type = get_meta_type(tag_name, tag)
        # TODO: don't forget to check language in html tag.
    # Deferred from web_page class because it's html's specific definition.
    # encuentra si el atributo es útil.
    # eliminar redundancias.
    # TODO: se puede mejorar el tiempo de procesamiento.
    def process_by_attributes(self, etree_tag, tag_type, languages={}):
        tag_attributes = etree_tag.attrib
        # pprint(tag_attributes)
        # for attribute, value in tag_attributes.items(): # causing trouble. maybe not.
        for attribute, value in tag_attributes.items():
        #for attribute in attributes:
            # print('attribute: '+attribute+', value: '+value)
            if attribute in directives:
                # calling to meta_directive corresponsal.
                # meta_directives[meta_attribute](meta_value, tag)
                '''
                function_name = meta_directives[attribute]
                function = getattr(self, function_name)
                function(attribute, value, tag)
                '''

                for meta_key in meta_key_words:
                    if value in meta_key_words[meta_key]:
                        element_class_name  = directives[attribute].capitalize()+'MetadataElement'
                        # https://stackoverflow.com/questions/17959996/get-python-class-object-from-class-name-string-in-the-same-module
                        element_class       = globals()[element_class_name]
                        attribute   = value
                        value       = tag_attributes['content']
                        metadata_element    = element_class(tag_type, attribute, value, etree_tag, languages)
                        # print('will create an element: '+ attribute+' with content: '+value)
                        self.save_metadata_element(metadata_element)
                        #self.directive

            elif re.match('\w:\w', attribute):
                # afirmo que es Rdfa.
                metadata_element    = RdfaMetadataElement(tag_type, attribute, value, etree_tag, languages)
                self.save_metadata_element(metadata_element)
            # else:
                # raise Exception("method should not be called.")
                # return 

    def get_common_element_content(self, tag_name):
        title_content = ''
        for md_element_type, md_elements in self._metadata_elements.items():
            if (md_element_type == 'common'):
                for md_element in md_elements:
                    if md_element.type == tag_name:
                        title_content = md_element.value
                        return title_content
        return title_content
