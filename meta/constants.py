
# meta tags, excluding title tag.
meta_key_words = {
# under the name attribute
    'title'         :
        ['subject', 'topic', 'pagename'],
    'additional'    : 
        ['description', 'abstract', 'summary'], 
    'categorization': 
        ['classification', 'keywords', 'category'],
}

# rating in meta_key_words looks somewhat interesing. <meta content='general' name='rating'/>

# Quizás no sea necesaria esta variable. TODO: Borrar
meta_key_regexp = {
    'rdfa'      : ['\w:\w'],
}

#todos los check generan un elemento metadato cuando es validado.
#COMMENT: el valor de la llave debe ser de una sóla palabra e incluir sólo letras.
directives = {
    'name'      : 'common', # general purpose.
    # RDFa Lite consists of five simple attributes:
    # vocab, typeof, property, resource, and prefix.
    ### 'property'  : 'rdfa',
    ### 'prefix'    : 'rdfa',
    ### 'vocab'     : 'rdfa',

    ### charsets, omitidos desde aquí y desde código.
    ### 'charset'   : 'common', 
    ### 'http-equiv': 'common', # can be content type with charset.

    # 'itemscope' : 'microdata', #quizás no considerar esto.
    ### 'itemtype'  : 'microdata',
    ### 'itemprop'  : 'microdata',

    #'lang'      : 'language',
    #'itemprop'  : obj.microdata,


}

# metadata_microdata text attributes:
         
 #  if 'href' in meta_tag.attrib:
 #      # 3b canonical web_pages
 #      item_property_value[ipv_index++] = meta_tag.attrib['href']
 #  if 'src' in meta_tag.attrib:
 #      item_property_value[ipv_index++] = meta_tag.attrib['src']
 #  if 'content' in meta_tag.attrib:
 #      item_property_value[ipv_index++] = meta_tag.attrib['content']
 #  elif 'value' in meta_tag.attrib:
 #      item_property_value[ipv_index++] = meta_tag.attrib['value']

text_attributes = ['href','src','content','value']


