#!/usr/bin/python3.5.0

import io
import sys

from web_page import WebPage
from meta.pdf_metadata import PdfMetadata


class PdfWebPage(WebPage):
    def __init__(self, url, response = None):
        self._metadata = PdfMetadata()
        self._response = response
        super().__init__(url)

    def show_metadata(self):
        self.metadata.show()

    def _parse_metadata(self, head):
        print("maybe I should use another tool than pdftotext.")
        self._metadata._parse()


    def _parse_body(self, head):
        print("maybe I should use another tool than pdftotext.")
        self._metadata._parse()

    def parse_document_content(self, content_variables):
        if not self._response: return None
        print("this is text without page numbers.")
        self._parse_metadata(head)
        # self._parse_body(body)

    def _define_title(self):
        print("get file name or title by metadata information")
        # self.title = title
        print("after deciding what my title may be, I return my title.")
        return 'this is my title'

    def _fetch_title(self):
        return 'my title'


    def _clean_content(self):
        print("without separators... guess")
        print("foo ha!, content cleaned without formatting must get out.")

    ### last added ###

    def print_metadata():
        print('this is my metadata.')
        
    def check_language_in_content(self):
        print('checking language by title definition.')