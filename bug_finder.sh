#! /bin/bash

file="output.txt" 
rm $file
touch $file
error=""

#!/bin/sh
#   if tail -n1 /var/log/messages | grep httpd; then
#     kdialog --msgbox "Apache needs love!"
#   fi
# done

#
# inotifywait -m output.txt -e create -e modify |
#     while read path action file; do
#         echo "The file '$file' appeared in directory '$path' via '$action'"
#         # do something with the file
#     done


echo 'executing bash script.'
while inotifywait -e modify $file; do
    if [ -s $file ]; then
	   echo '--------------'
        # build
        if grep -Fq "Traceback " $file; then 	
        	if [ -z "$error" ]; then
        		echo "A new bug was found."
	   		SECONDS=0
	   		error=$(tail -n 1 $file)
        	else
        		echo "Still working on the bug."
        		new_error=$(tail -n 1 $file)
        		error="$error ; $new_error"
        	fi
        	echo $error
        else
        	if [ -n "$error" ]; then
        		echo "The bug was fixed. Please save information."
        		echo $error
        		echo $SECONDS
        		error=""
        		# SECONDS=0
        	else 
        		echo "No bugs were found."
        	fi
        fi
    else
        echo "***file is empty***"
    fi
done