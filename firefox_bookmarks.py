
"""
tree = html.parse('http://www.datamystic.com/timezone/time_zones.html')
table = tree.findall('//table')[1]
data = [
           [td.text_content().strip() for td in row.findall('td')] 
           for row in table.findall('tr')
       ]
"""
from pprint import pprint as pp
from abc import ABCMeta, abstractmethod
class BrowserBookmarksParser(metaclass=ABCMeta):
	''' Trusts html browser correct html formatting. Or at least
	something that can be understood by lxml parser. See documentation.
	'''
	@abstractmethod
	def load_html_file(filename):
	    pass

#########################################################

import sys
from lxml import html
from lxml import etree

class FirefoxParser(BrowserBookmarksParser):

	def load_json_file(filename):
		pass

	def load_html_file(filename):
		data = []
		try:
			tree = html.parse(filename)
			# with open('out.txt', 'w') as f:
			# 	print(etree.tostring(tree, pretty_print=True), file=f)
			# print(tree.docinfo.encoding)
			# sys.exit(0)
		except Exception as error:
			print("file couldn't be parsed")
			data['error'] = error
		else: 
			user_links = tree.findall('.//dt/a')
			print(len(user_links))

			for index, link in enumerate(user_links):
				print("working on index: "+str(index))
				data_object 	= {}
				# pp(link.attrib.keys())

				if 'href' in link.attrib.keys():
					data_object['url'] = link.attrib['href']
					if data_object['url'].startswith('place'):
						continue

				if link.text:
					data_object['url_title'] = link.text
					
				if 'add_date' in link.attrib.keys():
					data_object['url_date_added'] = link.attrib['add_date']
				
				if 'icon_uri' in link.attrib.keys():
					data_object['url_icon_uri'] = link.attrib['icon_uri']

				if 'tags' in link.attrib.keys():
					data_object['url_tags'] 	= link.attrib['tags']


				# print(data_object['url'])
				dt_node = link.getparent()
				next_node = dt_node.getnext()
				# next_nodes = link.xpath('following-sibling::*')

				if (next_node is not None) and (next_node.tag == 'dd'):
					data_object['url_description'] = next_node.text
					# print('description: '+data_object['url_description'])

				dl_node = dt_node.getparent()
				while (dl_node is not None) and (dl_node.tag != 'dl'):
					dt_node = dl_node
					dl_node = dt_node.getparent()

				if (dl_node is not None) and (dl_node.tag == 'dl'):
					list_node 			= dl_node.getprevious()
					if list_node is not None:
						if list_node.tag == 'dd':
							list_description 	= list_node.text
							list_node 			= list_node.getprevious()
							data_object['list_description'] = list_description
						if list_node.tag == 'dt':
							# pp(type(list_node))
							try:
								node_element = list_node.find('.//h3')
								list_name = node_element.text
								data_object['list_name'] = list_name
							except:
								# no list name
								list_name = ''

				data.append(data_object)
		return data


	# los métodos de aqui abajo eran y quizas aun son de uso directo.
	def show_data(data_array):
		for index, data_object in enumerate(data_array):
			print('--index: '+str(index)+' --')
			for key in data_object.keys():
				print(key+' :'+data_object[key])

	def create_web_page_reference(data_array):
		''' It won't be validated url content. we trust what firefox has done
		and what the user has saved.
		'''
		for data_object in data_array:
			categorize = categorization.categorize(data_object['url'], False)			


if __name__ == '__main__': 
    # content = sys.argv
    data = FirefoxParser.load_html_file(sys.argv[1])
    FirefoxParser.show_data(data)