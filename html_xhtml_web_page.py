#!/usr/bin/python3.5.0

import io
import sys

from lxml import etree

from bm_traverse_modules.web_constants import (userAgentsDictionary, 
    userAgentsList)
from web_page import WebPage
from meta.constants import directives, meta_key_words
from meta.html_xhtml_metadata import HtmlXhtmlMetadata
from bm_traverse_modules.html_xhtml import (get_language_definition  
                          , check_language_consistence)
from console_constants   import (unicode_characters as uc, colors as c)
from pprint import pprint

# Do not validate if html is well-formed, bet it on the presence or absence of any datum in the tag. Difference between doctypes isn't that much.
# tested libraries: lxml, mechanize, beautifulSoup, url2
class HtmlXhtmlWebPage(WebPage):

    def __init__(self, url, response = None):
        self._metadata  = HtmlXhtmlMetadata()
        self._response  = response
        self._tree      = None
        self._parseable = None # boolean value
        super().__init__(url)

    def print_metadata(self):
        self._metadata.print_metadata_elements()

    # Checks if the root tag has language definition for it can be set as a whole for the web_page. 
    # If nothing's been said about the language create metadata information.
    def _parse_root_definition(self, root):
        lang_definition = get_language_definition(root)
        if lang_definition:
            print(c['green']+'MSG: Language defined in html tag content.'+c['end'])
            self._set_local_languages('html_tag', lang_definition)
            ### language_is_favorable = self.check_favorable_language()
            ### # False if one language was specified, but not english or spanish.
            ### if (language_is_favorable is False): return False
            ### # No language was specified, for the moment lets get out.
            ### # TODO: IN the future, try to guess language.
            ### elif (language_is_favorable is None): return None
        # Look for information as rdfa or microdata.
        # TODO: Remove. It's for a later version.
        self._metadata.process_etree_element(root)

    def guess_language_by_title(self):
        from langdetect import detect
        try:
            head_title_text = self._tree.find('head/title').text
            ### omitting for now
            ### language_detected = detect(head_title_text)
            ### self._set_local_languages('title_tag', language_detected)
            # will set alphabet by title content
            self._set_local_alphabet([head_title_text])
        except:
            print('MSG: '+c['red']+uc['wrong_checked']+c['end']+
                "Couldn't get title content to guess language.")


    def check_language_in_content(self):
        if not self._response: self.get_reponse()
        # print(str(self._parseable))
        if self._parseable is None:
            self.fetch_tree_element()
            return self.check_language_in_content()
        if self._parseable is False:
            # save data. Stop checking procedure.
            return None
        else: #TRUE, therefore self._tree exists.
            # check first lang definition in html tag.
            content_variables = {}
            content_variables['root'] = True
            content_variables['head'] = False
            self.parse_document_content(content_variables)
            if self.check_favorable_language() is not True:
                # Last chance: guess language by title content.
                self.guess_language_by_title()

    def _parse_metadata(self, head):
        meta_tags       = head.getchildren() # each child: etree.Element
        # print(len(meta_tags))
        for meta_tag in meta_tags:
            # Element or comment.
            self._metadata.process_etree_element(meta_tag)


    def _parse_body(self, body_element):
        print("Formatted html content must get in.")
        print("For the moment I'm done with this.")
        return ""

    # TODO: Create or handle an instance of parser by thread.
    # COMMENT: DO NOT verify consistency of content, it has already passed for the decision between HTML/XHTML or PDF. 
    #If it were another type of file like MP3 for example, it wouldn't have gotten until this method. 
    # Further it has been noticed web pages without body tag definition (first versions of HTML)
    # Tiene que terminar de ejecutar el metodo consiguiendo un etree element. sino abortar.
    def parse_document_content(self, content_variables={}):
        # This method does not return any value, instead it works on the web page variables,
        # thus running more than once will overwrite attribute values, default or previously generated.
        if not self._response: self.get_reponse()
        if not self._response: 
            print("has no response")
            return False # one last check before trying heavy stuff.

        if 'root' not in content_variables: content_variables['root'] = False
        if 'head' not in content_variables: content_variables['head'] = True
        if 'body' not in content_variables: content_variables['body'] = False

        # pprint(content_variables)

        if not self._tree:
            self.fetch_tree_element()

        if self._parseable is False:
            # an error ocurred. Tell main function to save_page_as_is
            return False
        else:
            # finding html definition
            if content_variables['root']:
                root_tag = self.fetch_root_element()
                self._parse_root_definition(root_tag)

            # finding and parsing head definition.
            if content_variables['head']:
                head_tag = self.fetch_head_element()
                if head_tag is not None:
                    print('head will be processed')
                    self._metadata.process_etree_element(head_tag)
                    self._parse_metadata(head_tag)

            # finding and parsing body definition.
            if content_variables['body']:
                # parse body as clean text (and html tags).
                self._parse_body(body)



    def fetch_tree_element(self):
        if self._encoding: 
            # exception added cause of facebook encoding :'b'"utf-8"''
            if 'utf-8' in self._encoding:
                self._encoding = 'utf-8' 
            encoding = self._encoding
        else: 
            encoding = 'utf-8' 
        parser  = etree.HTMLParser(encoding = encoding)
        try:
            self._tree = etree.parse(io.StringIO(self._response.text), parser)
            # adding to test
            # print(self._response.text)
            # print('se mostró el contenido')
            # print(etree.tostring(tree.getroot()))
            # print('se mostró la raiz')
        except Exception as e:
            # by some reason it can't be parsed. One known reason is unicode charset.
            print("MSG: "+c['red']+uc['wrong_checked']+c['end']+
                "encoding and parsing error with: "+self._url)
            # print("Return command/direction to user.")
            print(e)
            self._parseable = False
        else:
            self._parseable = True

    def fetch_root_element(self):
        root = self._tree.getroot()
        if (root.tag == 'html'):
            html_tag = root
        else:
            html_tag = root.find('html')
            if (html_tag == None):
                print("MSG: web page has no html definition.")
                return None
        return html_tag
        # parsing html tags
        # print(root.attrib)
        # for child in root:
        #     print(child.tag, child.attrib)
        # with open('testfile.txt', 'wb') as file:
        #     output = etree.tostring(root, pretty_print=True, method='html')
        #     #print(type(output))
        #     # sys.exit(0)
        #     file.write(output)

    def fetch_head_element(self):
        head = self._tree.find('head') # etree.ElementTree
        if (head == None):
            print("MSG: web page has no head definition.")
            return None
        return head


    def fetch_body_element(self):
        body = self._tree.find('body') # etree.ElementTree
        if (body == None):
            print("MSG: web page has no body definition.")
            return None
        return body
        


    def _fetch_title(self):
        print("get h headers")
        # self.title = title
        print("after deciding what my title may be, I return my title.")
        return 'this is my title'

    def _clean_content(self):
        print("foo ha!, content cleaned without formatting must get out.")

    def __str__(self):
        return "HtmlXhtmlWebPage"

    # public, deprecated
    def get_title_content(self):
        return self._metadata.get_common_element_content('title')

    # public
    def get_title_contents(self):
        contents = []
        main_title = self._metadata.get_common_element_content('title')
        if main_title is not '': contents.append(main_title)

        for meta_type in meta_key_words['title']:
            title = self._metadata.get_common_element_content(meta_type)
            if title is not '': contents.append(title)
        return contents

    # public
    def get_additional_contents(self):
        contents = []
        for meta_type in meta_key_words['additional']:
            additional = self._metadata.get_common_element_content(meta_type)
            if additional is not '': contents.append(additional)
        return contents

    # public
    def get_categorization_contents(self):
        contents = []
        for meta_type in meta_key_words['categorization']:
            categorization = self._metadata.get_common_element_content(meta_type)
            if categorization is not '': contents.append(categorization)
        return contents